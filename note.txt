##> Estou usando bootstrap para facilitar a confecção da view, não sei se vocês usam algum framework css
ou usam css nativo (Flex Box ou display grid por exemplo)

##> Não fiz nenhuma validação backend pela questão do tempo, adicionei apenas um required na tag input do form

OK Laravel 5.6
OK Seed Users
OK id, name, email, password, city, state, postal_code, address, number, complement, district
OK Authentication Page
OK Product Page
OK User Info Page

OK CRUD Product
OK Title, Description, Image, Thumbnail and Price (use mask)
 
- UnitTest (+)

OK HTML + CSS
- VueJS (+)
OK use ajax to complete user address after input postal_code (bonus point if use VueJs)

php artisan migrate:refresh --seed
php artisan db:seed --class=UsersTableSeeder
composer dump-autoload