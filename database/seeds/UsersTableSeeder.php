<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Lara Vel',
            'email' => 'laravel@gmail.com',
            'password' => bcrypt('laravel'),
            'city' => 'Guarapuava',             
            'state' => 'Paraná',             
            'postal_code' => '85040-030',             
            'address' => 'Rua Aymoré',             
            'number' => '123',             
            'complement' => 'Sem complemento',                         
            'district' => 'Vila Carli'                                     
        ]);
    }
}
