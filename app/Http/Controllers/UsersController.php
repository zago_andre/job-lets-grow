<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = \App\Users::all();       
        return view('users/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $user = new \App\Users;
        $user->name = $request->get('name');        
        $user->email = $request->get('email');                
        $user->password = bcrypt($request->get('password'));                
        $user->city = $request->get('city');                
        $user->state = $request->get('state');                            
        $user->postal_code = $request->get('postal_code');                                                     
        $user->address = $request->get('address');                                                     
        $user->number = $request->get('number');                                                     
        $user->complement = $request->get('complement');                                                     
        $user->district = $request->get('district');   
        $user->save();

        return redirect('users')->with('success', 'Usuário salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \App\Users::find($id);        
        return view('users/show',compact('user','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\Users::find($id);        
        return view('users/edit',compact('user','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = \App\Users::find($id);
        $user->name = $request->get('name');        
        $user->email = $request->get('email');                                    
        $user->city = $request->get('city');                
        $user->state = $request->get('state');                            
        $user->postal_code = $request->get('postal_code');                                                     
        $user->address = $request->get('address');                                                     
        $user->number = $request->get('number');                                                     
        $user->complement = $request->get('complement');                                                     
        $user->district = $request->get('district');   
        $user->save();

        return redirect('users')->with('success', 'Usuário salvo com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \App\Users::find($id);
        $user->delete();
        return redirect('users')->with('success','Usuário excluído com sucesso');
    }
}
