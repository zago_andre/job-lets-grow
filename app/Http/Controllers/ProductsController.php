<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = \App\Products::all();       
        return view('products/index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasfile('image')){
            $file_image = $request->file('image');
            $name_image = time() . $file_image->getClientOriginalName();
            $file_image->move(public_path().'/images/', $name_image);
        }

        if($request->hasfile('thumbnail')){
            $file_thumb = $request->file('thumbnail');
            $name_thumb = time() . $file_thumb->getClientOriginalName();
            $file_thumb->move(public_path().'/images/', $name_thumb);
        }

        $product = new \App\Products;
        $product->title = $request->get('title');        
        $product->description = $request->get('description');        
        $product->price = $request->get('price');        
        $product->image = $name_image;
        $product->thumbnail = $name_thumb;
        $product->save();
        
        return redirect('products')->with('success', 'Produto salvo com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = \App\Products::find($id);        
        return view('products/show',compact('product','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = \App\Products::find($id);        
        return view('products/edit',compact('product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = \App\Products::find($id);

        if($request->hasfile('image')){
            $file_image = $request->file('image');
            $name_image = time() . $file_image->getClientOriginalName();
            $file_image->move(public_path().'/images/', $name_image);
            $product->image = $name_image;
        }

        if($request->hasfile('thumbnail')){
            $file_thumb = $request->file('thumbnail');
            $name_thumb = time() . $file_thumb->getClientOriginalName();
            $file_thumb->move(public_path().'/images/', $name_thumb);
            $product->thumbnail = $name_thumb;
        }

        $product->title = $request->get('title');        
        $product->description = $request->get('description');        
        $product->price = $request->get('price');        
        $product->save();
        
        return redirect('products')->with('success', 'Produto salvo com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = \App\Products::find($id);
        $product->delete();
        return redirect('products')->with('success','Produto excluído com sucesso');
    }
}