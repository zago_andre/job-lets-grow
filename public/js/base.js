window.onload = function(){	
	//Mask Money
	$("#inputPrice").maskMoney({thousands:'', decimal:'.', allowZero:true});

	var inputPostalcode = document.getElementById("inputPostalcode");
	
	if(inputPostalcode){
		inputPostalcode.addEventListener("focusout", inputPostalcodeFocusOut);	
	}

	function inputPostalcodeFocusOut() {
		var cep = document.getElementById('inputPostalcode').value;
		
		fetch("https://viacep.com.br/ws/" + cep + "/json/")
			.then(response => response.json()) 
		  	.then(result => {
		  		document.getElementById('inputCity').value = result.localidade;
		  		document.getElementById('inputState').value = result.uf;
		  		document.getElementById('inputAddress').value = result.logradouro;
		  		document.getElementById('inputComplement').value = result.complemento;
		  		document.getElementById('inputDistrict').value = result.bairro;
		    	console.log(result);
		  	});    
	}  
}

