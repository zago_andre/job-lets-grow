@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
	<h4>Usuário :. Novo</h2>
    <form method="post" action="{{url('users')}}" id="userForm" enctype="multipart/form-data">
    	<div class="form-group">
    		<label for="inputName">Nome</label>
   			<input type="text" name="name" class="form-control" id="inputName" placeholder="Nome" required>
  		</div>
  		<div class="form-row">
    		<div class="form-group col-md-6">
      			<label for="inputEmail">Email</label>
      			<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" required>
    		</div>
    		<div class="form-group col-md-6">
      			<label for="inputPassword">Password</label>
      			<input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password" required>
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="inputPostalcode">CEP</label>
   			<input type="text" name="postal_code" class="form-control" id="inputPostalcode" placeholder="CEP" required>
  		</div>
  		<div class="form-row">
    		<div class="form-group col-md-6">
	    		<label for="inputCity">Cidade</label>
	   			<input type="text" name="city" class="form-control" id="inputCity" placeholder="Cidade" required>
	   		</div>
    		<div class="form-group col-md-6">
	    		<label for="inputState">Estado</label>
	    		<input type="text" name="state" class="form-control" id="inputState" placeholder="Estado" required>
	    	</div>
  		</div>  		
  		<div class="form-row">
    		<div class="form-group col-md-9">
	    		<label for="inputAddress">Endereço</label>
	   			<input type="text" name="address" class="form-control" id="inputAddress" placeholder="Endereço" required>
	   		</div>
    		<div class="form-group col-md-3">
	    		<label for="inputNumber">Número</label>
	    		<input type="text" name="number" class="form-control" id="inputNumber" placeholder="Número" required>
	    	</div>
  		</div> 
  		<div class="form-row">
    		<div class="form-group col-md-6">
	    		<label for="inputComplement">Complemento</label>
	   			<input type="text" name="complement" class="form-control" id="inputComplement" placeholder="Complemento" required>
	   		</div>
    		<div class="form-group col-md-6">
	    		<label for="inputDistrict">Bairro</label>
	    		<input type="text" name="district" class="form-control" id="inputDistrict" placeholder="Bairro" required>
	    	</div>
  		</div>  
  		{{ csrf_field() }}		
  		
  		<button type="submit" class="btn btn-primary">Enviar</button>
	</form>
@endsection