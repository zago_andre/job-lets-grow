@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
	<h4>Usuário :. Visualização</h2>
    <div>
        <address>
            <strong>{{$user['name']}}</strong><br>
            {{$user['email']}}<br>
            {{$user['city']}} - {{$user['state']}} <br>
            CEP: {{$user['postal_code']}} <br>
            Endereço: {{$user['address']}}, {{$user['number']}}, {{$user['complement']}}, {{$user['district']}} <br>
        </address>        
    </div>
@endsection