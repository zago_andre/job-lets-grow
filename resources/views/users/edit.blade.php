@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
    <h4>Usuário :. Edição</h2>
    <form method="post" action="{{action('UsersController@update', $id)}}">
        {{ method_field('PUT')}}  
  		{{ csrf_field() }}		
        <div class="form-group">
            <label for="inputName">Nome</label>
            <input type="text" name="name" class="form-control" id="inputName" value="{{$user['name']}}" placeholder="Nome" required>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail">Email</label>
                <input type="email" name="email" class="form-control" id="inputEmail" value="{{$user['email']}}" placeholder="Email" required>
            </div>  
            <div class="form-group col-md-6">
                <label for="inputPostalcode">CEP</label>
            <input type="text" name="postal_code" class="form-control" id="inputPostalcode" value="{{$user['postal_code']}}" placeholder="CEP" required>
            </div>          
        </div>          
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">Cidade</label>
                <input type="text" name="city" class="form-control" id="inputCity" value="{{$user['city']}}" placeholder="Cidade" required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputState">Estado</label>
                <input type="text" name="state" class="form-control" id="inputState" value="{{$user['state']}}" placeholder="Estado" required>
            </div>
        </div>          
        <div class="form-row">
            <div class="form-group col-md-9">
                <label for="inputAddress">Endereço</label>
                <input type="text" name="address" class="form-control" id="inputAddress" value="{{$user['address']}}" placeholder="Endereço" required>
            </div>
            <div class="form-group col-md-3">
                <label for="inputNumber">Número</label>
                <input type="text" name="number" class="form-control" id="inputNumber" value="{{$user['number']}}" placeholder="Número" required>
            </div>
        </div> 
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputComplement">Complemento</label>
                <input type="text" name="complement" class="form-control" id="inputComplement" value="{{$user['complement']}}" placeholder="Complemento" required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputDistrict">Bairro</label>
                <input type="text" name="district" class="form-control" id="inputDistrict" value="{{$user['district']}}" placeholder="Bairro" required>
            </div>
        </div>  
  		
  		<button type="submit" class="btn btn-primary">Enviar</button>
	</form>
@endsection