@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
    @if (\Session::has('success'))       
        <div class="alert alert-success" role="alert">
            {{ \Session::get('success') }}
        </div>
    @endif

    <h4>Usuários :. Listagem</h2>

    <table class="table table-striped">
        <thead>
            <tr>
                <th width="15%" scope="col">Nome</th>                
                <th width="15%" scope="col">Email</th>                
                <th width="15%" scope="col">Cidade</th>                
                <th width="15%" scope="col">Estado</th>                
                <th width="15%" scope="col">Cep</th>                
                <th width="15%" scope="col">Endereço</th>                
                <th width="10%" colspan="3" scope="col">Açoes</th>                
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)      
                <tr>
                    <td>{{$user['name']}}</td>                    
                    <td>{{$user['email']}}</td>                    
                    <td>{{$user['city']}}</td>                    
                    <td>{{$user['state']}}</td>                    
                    <td>{{$user['postal_code']}}</td>                    
                    <td>{{$user['address']}}, {{$user['number']}}, {{$user['complement']}}, {{$user['district']}}</td>                    
                    <td><a href="{{action('UsersController@show', $user['id'])}}" class="btn btn-primary btn-sm">Visualizar</a></td>                    
                    <td><a href="{{action('UsersController@edit', $user['id'])}}" class="btn btn-primary btn-sm">Editar</a></td>                    
                    <td>
                        <form action="{{action('UsersController@destroy', $user['id'])}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Tem certeza que deseja deletar este item?');">Deletar</button>
                        </form>
                    </td>                    
                </tr>
            @endforeach             
        </tbody>
    </table>      
@endsection