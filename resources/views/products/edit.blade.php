@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
	<h4>Produto :. Edição</h2>
    <form method="post" action="{{action('ProductsController@update', $id)}}" enctype="multipart/form-data">    
        {{ method_field('PUT')}}  
        {{ csrf_field() }}

    	<div class="form-group">
    		<label for="inputTitle">Título</label>
   			<input type="text" name="title" class="form-control" value="{{$product['title']}}" id="inputTitle" placeholder="Título" required>
  		</div>
        <div class="form-group">
            <label for="inputDescription">Descrição</label>
            <input type="textarea" name="description" class="form-control" value="{{$product['description']}}" id="inputDescription" placeholder="Descrição" required>
        </div>
        <div class="form-group">
            <label for="inputPrice">Preço</label>
            <input type="text" name="price" class="form-control" value="{{$product['price']}}" id="inputPrice" placeholder="Preço" required>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputImage">Imagem</label>
                <input type="file" name="image" class="form-control-file" id="inputImage"> 
                <img src="/images/{{$product['image']}}" alt="Image" class="img-thumbnail">          
            </div>
            <div class="form-group col-md-6">
                <label for="inputThumbnail">Thumbnail</label>
                <input type="file" name="thumbnail" class="form-control-file" id="inputThumbnail">  
                <img src="/images/{{$product['thumbnail']}}" alt="Image" class="img-thumbnail">          
            </div>
        </div>       
  		<button type="submit" class="btn btn-primary">Enviar</button>
	</form>
@endsection