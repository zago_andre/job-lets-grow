@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
	<h4>Produto :. Visualização</h2>
    <div>
        <address>
            <strong>{{$product['title']}}</strong><br>
            {{$product['description']}}<br>
            R$ {{ number_format($product['price'], 2, ',', '.') }}<br>            
        </address>
        <img src="/images/{{$product['image']}}" alt="Image" class="img-thumbnail">          
        <img src="/images/{{$product['thumbnail']}}" alt="Image" class="img-thumbnail">          
    </div>
@endsection