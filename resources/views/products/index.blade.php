@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
    @if (\Session::has('success'))       
        <div class="alert alert-success" role="alert">
            {{ \Session::get('success') }}
        </div>
    @endif

    <h4>Produtos :. Listagem</h2>

    <table class="table table-striped">
        <thead>
            <tr>                
                <th width="30%" scope="col">Título</th>                                             
                <th width="30%" scope="col">Descrição</th>                                             
                <th width="30%" scope="col">Preço</th>
                <th width="10%" colspan="3" scope="col">Açoes</th>                                                  
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)      
                <tr>
                    <td>{{$product['title']}}</td>                                              
                    <td>{{$product['description']}}</td>                                              
                    <td>R$ {{ number_format($product['price'], 2, ',', '.') }}</td>  
                    <td><a href="{{action('ProductsController@show', $product['id'])}}" class="btn btn-primary btn-sm">Visualizar</a></td>                    
                    <td><a href="{{action('ProductsController@edit', $product['id'])}}" class="btn btn-primary btn-sm">Editar</a></td>                    
                    <td>
                        <form action="{{action('ProductsController@destroy', $product['id'])}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Tem certeza que deseja deletar este item?');">Deletar</button>
                        </form>
                    </td>                                                
                </tr>
            @endforeach             
        </tbody>
    </table>      
@endsection