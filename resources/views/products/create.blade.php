@extends('layouts.app')

@section('title', 'Let´s Grow')

@section('content')
	<h4>Produto :. Novo</h2>
    <form method="post" action="{{url('products')}}" enctype="multipart/form-data">
    	<div class="form-group">
    		<label for="inputTitle">Título</label>
   			<input type="text" name="title" class="form-control" id="inputTitle" placeholder="Título" required>
  		</div>
        <div class="form-group">
            <label for="inputDescription">Descrição</label>
            <input type="textarea" name="description" class="form-control" id="inputDescription" placeholder="Descrição" required>
        </div>
        <div class="form-group">
            <label for="inputPrice">Preço</label>
            <input type="text" name="price" class="form-control" id="inputPrice" placeholder="Preço" required>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputImage">Imagem</label>
                <input type="file" name="image" class="form-control-file" id="inputImage" required>           
            </div>
            <div class="form-group col-md-6">
                <label for="inputThumbnail">Thumbnail</label>
                <input type="file" name="thumbnail" class="form-control-file" id="inputThumbnail" required>  
            </div>
        </div>       
  		
  		{{ csrf_field() }}		
  		
  		<button type="submit" class="btn btn-primary">Enviar</button>
	</form>
@endsection